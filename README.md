# WebView Interstitials for AATKit (iOS)

This demo project shows how to integrate AATKit into a web based HTML5 application using `WKWebView`.

## Installation

Before you can use this project, you need to integrate the current version of AATKit. Download and extract the latest version of AATKit from AddApptr:

    rm -rf aat; curl -O 'https://download.addapptr.com/AATKit.zip' && unzip AATKit.zip

## Build and Execute

Now, you can open the `WebViewInterstitial.xcodeproj` with your IDE to build and run the application.

