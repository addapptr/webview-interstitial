var AATKit = (function () {
    var AATKit = document.createElement('AATKit');
    var version = '2016-11-09';
    
    AATKit.showInterstitial = function showInterstitial () {
        window.webkit.messageHandlers.aat.postMessage({event: 'showInterstitial', version: version});
    };
              
    return AATKit;
})()
