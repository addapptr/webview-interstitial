//
//  FirstViewController.swift
//  WebViewInterstitials
//
//  Created by Sven Herzberg on 09.11.16.
//  Copyright © 2016 AddApptr GmbH. All rights reserved.
//

import UIKit
import WebKit

class FirstViewController: UIViewController {
    
    var placement: Any!
    var webView: WKWebView!
    
    deinit {
        if let placement = placement {
            AATKit.setPlacementViewController(nil, for: placement as! AATKitPlacement)
        }
    }

    // Do any additional setup after loading the view, typically from a nib.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let placementName = "interstitialForHTML5"
        placement = AATKit.getPlacementWithName(placementName)
        if placement == nil {
            placement = AATKit.createPlacement(withName: placementName, andType: .fullscreen)
        }
        
        let aatKitJS = Bundle.main.url(forResource: "aatkit", withExtension: "js")!
        let javaScript = try! String(contentsOf: aatKitJS, encoding: .utf8)
        let userScript = WKUserScript(source: javaScript, injectionTime: .atDocumentStart, forMainFrameOnly: true)
        
        let config = WKWebViewConfiguration()
        config.applicationNameForUserAgent = "WebViewInterstitials"
        config.userContentController.add(self, name: "aat")
        config.userContentController.addUserScript(userScript)
        
        let buttonHTML = Bundle.main.url(forResource: "button", withExtension: "html")!
        webView = WKWebView(frame: CGRect.zero, configuration: config)
        webView.loadFileURL(buttonHTML, allowingReadAccessTo: buttonHTML)
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(webView)
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-(0)-[webView]-(0)-|", options: [], metrics: nil, views: ["webView": webView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[webView]-(0)-|", options: [], metrics: nil, views: ["webView": webView]))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AATKit.setDelegate(self)
        AATKit.startPlacementAutoReload(placement as! AATKitPlacement)
        AATKit.setPlacementViewController(self, for: placement as! AATKitPlacement)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AATKit.setDelegate(nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension FirstViewController: AATKitDelegate {
    
    func aatKitPauseForAd() {
        print("### pause for ad ###")
        webView.evaluateJavaScript("(function () {var event = new Event('pauseForAd'); AATKit.dispatchEvent(event);})()", completionHandler: nil)
    }
    
    func aatKitResumeAfterAd() {
        print("### resume after ad ###")
        webView.evaluateJavaScript("(function () {var event = new Event('resumeAfterAd'); AATKit.dispatchEvent(event);})()", completionHandler: nil)
    }
    
}

extension FirstViewController: WKScriptMessageHandler {
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        guard let body = message.body as? [String: Any] else {
            return
        }
        
        guard let event = body["event"] as? String else {
            return
        }
        
        guard let version = body["version"] as? String else {
            return
        }
        
        print("WebViewInterstitial: received event “\(event)” with version “\(version)”")
        
        switch event {
        case "showInterstitial":
            if !AATKit.show(placement as! AATKitPlacement) {
                print("No fullscreen ad was available.")
            }
        default:
            return
        }
    }
    
}
