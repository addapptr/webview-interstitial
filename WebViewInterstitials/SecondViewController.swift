//
//  SecondViewController.swift
//  WebViewInterstitials
//
//  Created by Sven Herzberg on 09.11.16.
//  Copyright © 2016 AddApptr GmbH. All rights reserved.
//

import UIKit
import WebKit

class SecondViewController: UIViewController {
    
    var adContainer: UIView!
    var placement: Any!
    var webView: WKWebView!

    // Do any additional setup after loading the view, typically from a nib.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let placementName = "bannerHTML5"
        placement = AATKit.getPlacementWithName(placementName)
        if placement == nil {
            placement = AATKit.createPlacement(withName: placementName, andType: .banner320x53)
        }
        
        webView = WKWebView(frame: CGRect.zero)
        webView.loadHTMLString("<!doctype html><html><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no'><style>body {background-color: maroon; color: white; font-family: sans-serif; margin-top: 2em;}</style>Hello, World!", baseURL: nil)
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        adContainer = UIView()
        adContainer.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(webView)
        view.addSubview(adContainer)
        
        let views: [String: Any] = [
            "adContainer": adContainer,
            "bottom": bottomLayoutGuide,
            "top": topLayoutGuide,
            "webView": webView,
        ]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[webView]-(0)-|", options: [], metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[adContainer(>=320@1000)]-(0)-|", options: [], metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[webView]-(0)-[adContainer(==53)]-(0)-[bottom]", options: [], metrics: nil, views: views))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AATKit.setPlacementViewController(self, for: placement as! AATKitPlacement)
        AATKit.startPlacementAutoReload(placement as! AATKitPlacement)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        adContainer.addSubview(AATKit.getPlacementView(placement as! AATKitPlacement)!)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        AATKit.setPlacementViewController(nil, for: placement as! AATKitPlacement)
        AATKit.stopPlacementAutoReload(placement as! AATKitPlacement)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
